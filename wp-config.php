<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'rainman');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`;&c[g@;mbS$RdsKCZ]r<N}JRvb_xfEfO]1,=fc#=L562P+p<FL{uzy6B4YdR^S/');
define('SECURE_AUTH_KEY',  '2$eeuSgY~gW*cW g$4,KPa? G/|gl6?H5^u~D8Qkd;5GQ]dw<#Y>DHFX{~Gmh8);');
define('LOGGED_IN_KEY',    'ku)]yW1XjTzn7RwqHad?~76dVT+hvtJuPIXnL+P y}[0x;:`n[d#/[k8B]js*$LD');
define('NONCE_KEY',        ']M<8;8AqDneL]^SQK<A)[SUaV/wwIk{}B_j2UKRA2&F~g$mq@}}xmioa9#S&=e&S');
define('AUTH_SALT',        '-#Hoi0<C|Sg-CZ$!,6-0DdL^=l7@m=(j@Jn1w;;<<3!w4)haI86rdq!!ks@kSd&k');
define('SECURE_AUTH_SALT', 'fn|W)X_$8e~+En$YK1-2:S;rH0yAcl,P0Y`no.(+l5YmM=P>pktXi{h!K[cN$HP7');
define('LOGGED_IN_SALT',   ':OIH)r*q@u6[K0^kK%TM~7=l*Txfox2tTOd`gU!J9K{Yp4o$%v03XhT~H!S|ZD&r');
define('NONCE_SALT',       'k.wh3Q:P7DR>9BS2RbZq mZ0h~MrcDy3sOt[7He/g|SuTHrW%4U)Z^a4q5-d:+v^');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
